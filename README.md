## Digital Republic Code Challenge

Aplicação feita com intuito de gerenciar custos e despesas para pintar um ambiente. O usuário deve informar medidas para as 4 paredes do ambiente e quantas janelas e portas cada parede possui. A partir dos dados inputados, é calculada a área total para pintura (área total das paredes desconsiderando paredes e janelas) e retornado quantas latas de tinta de cada litragem deve ser utilizada para suprir a pintura do ambiente.


# Sumário
1. <a href="#Hosted-APP">Hosted APP</a>
2. <a href="#Documentação-da-Aplicação">Documentação da Aplicação</a>
3. <a href="#Tecnologias-utilizadas">Tecnologias Utilizadas</a>
4. <a href="#Inicializando">Inicializando</a>
5. <a href="#Configurando-o-Projeto">Configurando o Projeto</a>
6. <a href="#Gerando-e-Rodando-Migrations-(TypeORM)">Gerando e Rodando Migrations (TypeORM)</a>
7. <a href="#Rodando-Testes">Rodando Testes</a>
8. <a href="#Deploy">Deploy</a>
9. <a href="#CI/CD">CI/CD</a>
10. <a href="#API-Endpoints">API Endpoints</a>
11. <a href="#Autor">Autor</a>

## Hosted APP

https://digitalrepublic-code-challenge.herokuapp.com/

## Documentação da Aplicação

https://digitalrepublic-code-challenge.herokuapp.com/api-docs

## Tecnologias Utilizadas

- [NodeJS](https://nodejs.org/)
- [Typescript](https://www.typescriptlang.org/)
- [Jest](https://jestjs.io/)
- [Supertest]()

## Configurando o Projeto

Setar porta de inicialização da aplicação para rodar localmente

|     Variável    |      Default     |          Notes           |
| --------------- | ---------------- | ------------------------ |
|     `PORT`      |      `3600`      |  Porta de inicialização  |

## Inicializando

- Clonar o repositório: `git clone https://gitlab.com/vianagustavo/digital-republic-code-challenge.git`
- Instalar dependências `npm ci`

 Há a alternativa de incializar a aplicação via Docker:

- Buildar imagem da aplicação: `docker build -t digital-republic-code-challenge .`
- Executar docker-compose: `docker-compose up -d`

## Rodando Testes

Os testes de integração estão disponíveis para todos os endpoints da aplicação, e o script utilizado para o rodar o Jest pode ser encontrado no `package.json`.

```
# Rodando os testes
$ npm run test

```
## CI/CD

Aproveitando a iniciativa de utilizar o deploy na plataforma do Heroku, também foram utilizados os conceitos de CI/CD, através da Pipeline do próprio GitLab, sempre que for feito um push para a branch main, adotando boas práticas de desenvolvimento e automação da implantação da nossa aplicação.

O workflow completo pode ser encontrado em: 

``` .gitlab-ci.yml ```

## API Endpoints

|  Verbo   |                    Endpoint                     |                 Descrição                  | 
| :------- | :---------------------------------------------: | :----------------------------------------: |
| `POST`   |                    `/calculate`                 |            Cálculo de gastos               |

## Autor

- **Gustavo Ferreira Viana**