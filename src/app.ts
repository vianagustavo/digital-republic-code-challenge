import express from "express";
import "reflect-metadata";
import "express-async-errors";
import { errorHandler } from "./middlewares/errorHandler";
import { router } from "./routes";
import swaggerUi from "swagger-ui-express";
import swaggerFile from "./swagger.json";

const app = express();

app.use(express.json());

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerFile));

app.use(router);

app.use(errorHandler);

export default app;
