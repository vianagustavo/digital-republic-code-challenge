import { Request, Response } from "express";
import { ICalculateRequest } from "../domain/requestDto";
import { CalculateExpensesService } from "../services/calculateExpensesService";

class CalculateExpensesController {
  handle(request: Request, response: Response) {
    const wallMeasurements: ICalculateRequest = request.body;
    const calculateExpensesService = new CalculateExpensesService();
    const cansQuantity = calculateExpensesService.execute(wallMeasurements);
    return response.json(cansQuantity);
  }
}

export { CalculateExpensesController };
