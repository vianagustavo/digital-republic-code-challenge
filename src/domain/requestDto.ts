export interface IWallParameters {
  length: number;
  height: number;
  nDoor: number;
  nWindow: number;
}

export interface ICalculateRequest {
  wallMeasurements: IWallParameters[];
}

export interface ICalculateResponse {
  literage: number;
  quantity: number;
}
