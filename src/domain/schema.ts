import { object, z } from "zod";

export const wallMeasurement = z.object({});

export type WallMeasurement = z.infer<typeof wallMeasurement>;

export const calculateBody = object({
  body: z.object({
    wallMeasurements: z
      .array(
        z.object({
          length: z.number(),
          height: z.number(),
          nDoor: z.number(),
          nWindow: z.number(),
        })
      )
      .length(4),
  }),
});

export type CalculateBody = z.infer<typeof calculateBody>;
