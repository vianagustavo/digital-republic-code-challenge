import { Response, Router } from "express";
import { CalculateExpensesController } from "./controllers/calculateExpensesController";
import { calculateBody } from "./domain/schema";
import validateResource from "./middlewares/requestValidator";

const router = Router();

const calculateExpensesController = new CalculateExpensesController();

router.get("/", (_, response: Response) => {
  return response.json({
    ok: true,
  });
});

router.post(
  "/calculate",
  validateResource(calculateBody),
  calculateExpensesController.handle
);

export { router };
