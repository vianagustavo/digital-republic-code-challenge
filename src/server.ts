import "reflect-metadata";
import app from "./app";

try {
  app.listen(process.env.PORT, () =>
    console.log(`Server is running on port (${process.env.PORT})`)
  );
} catch (err) {
  console.log(err);
}
