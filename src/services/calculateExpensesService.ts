import { DOOR_AREA, WINDOW_AREA } from "../domain/ constants";
import { InvalidArgument } from "../domain/error";
import { ICalculateResponse, ICalculateRequest } from "../domain/requestDto";

function checkMeasuresRestrictions(
  data: ICalculateRequest,
  doorArea: number,
  windowArea: number
) {
  for (const currentWallMeasurements of data.wallMeasurements) {
    if (
      currentWallMeasurements.nDoor > 0 &&
      currentWallMeasurements.height < 2.2
    ) {
      throw new InvalidArgument(
        "Wall height must be 30cm bigger than Door height"
      );
    }

    const wallArea =
      currentWallMeasurements.height * currentWallMeasurements.length;
    if (wallArea < 1 || wallArea > 50) {
      throw new InvalidArgument(
        "Walls can't have less than 1m² or more than 50m²"
      );
    }

    if (
      doorArea * currentWallMeasurements.nDoor +
        windowArea * currentWallMeasurements.nWindow >
      wallArea / 2
    ) {
      throw new InvalidArgument(
        "Door + Window area must not cover 50%+ of Wall area"
      );
    }
  }
}

function getCansQuantity(
  totalPaintingVolume: number,
  canSizes: number[]
): ICalculateResponse[] {
  let remainder = totalPaintingVolume;
  const cansQuantity: ICalculateResponse[] = [];

  for (const size of canSizes) {
    cansQuantity.push({
      literage: size,
      quantity: Math.floor(remainder / size),
    });
    remainder = remainder % size;
  }

  if (remainder > 0) {
    cansQuantity[cansQuantity.length - 1].quantity++;
  }
  return cansQuantity;
}

function getTotalPaintingVolume(
  data: ICalculateRequest,
  doorArea: number,
  windowArea: number
) {
  let totalPaintingArea = 0;
  for (const currentWallMeasurements of data.wallMeasurements) {
    const wallArea =
      currentWallMeasurements.height * currentWallMeasurements.length;
    const totalWindowDoorArea =
      currentWallMeasurements.nDoor * doorArea +
      currentWallMeasurements.nWindow * windowArea;

    totalPaintingArea += wallArea - totalWindowDoorArea;
  }

  return totalPaintingArea / 5;
}

class CalculateExpensesService {
  execute(data: ICalculateRequest): ICalculateResponse[] {
    checkMeasuresRestrictions(data, DOOR_AREA, WINDOW_AREA);
    const totalPaintingVolume = getTotalPaintingVolume(
      data,
      DOOR_AREA,
      WINDOW_AREA
    );
    const canSizes = [18, 3.6, 2.5, 0.5];

    const cansQuantity = getCansQuantity(totalPaintingVolume, canSizes);
    return cansQuantity;
  }
}

export { CalculateExpensesService };
