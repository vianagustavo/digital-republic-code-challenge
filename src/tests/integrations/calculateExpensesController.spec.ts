import supertest from "supertest";
import app from "../../app";
import { ICalculateRequest, ICalculateResponse } from "../../domain/requestDto";

describe("Calculate Expenses Controller", () => {
  it("Should be able to calculate expenses", async () => {
    const data: ICalculateRequest = {
      wallMeasurements: [
        {
          length: 3,
          height: 15,
          nDoor: 0,
          nWindow: 0,
        },
        {
          length: 2,
          height: 5,
          nDoor: 0,
          nWindow: 0,
        },
        {
          length: 2,
          height: 15,
          nDoor: 0,
          nWindow: 0,
        },
        {
          length: 2,
          height: 15,
          nDoor: 0,
          nWindow: 0,
        },
      ],
    };

    const response = await supertest(app).post("/calculate").send(data);
    expect(response.status).toBe(200);
    const responseBody: ICalculateResponse[] = response.body;
    const expectedLiterage = [0.5, 18];
    const match = responseBody.filter((c) => {
      expectedLiterage.includes(c.literage);
    });

    expect(
      match.every((m) => {
        m.quantity > 0;
      })
    ).toBeTruthy();
  });

  it("should fail due to insufficient wall measures", async () => {
    const data: ICalculateRequest = {
      wallMeasurements: [
        {
          length: 2,
          height: 15,
          nDoor: 0,
          nWindow: 0,
        },
      ],
    };
    const response = await supertest(app).post("/calculate").send(data);
    expect(response.status).toBe(400);
  });

  it("should fail if wall height isn't 30cm bigger than door height", async () => {
    const data: ICalculateRequest = {
      wallMeasurements: [
        {
          length: 2,
          height: 2,
          nDoor: 1,
          nWindow: 0,
        },
        {
          length: 2,
          height: 5,
          nDoor: 0,
          nWindow: 0,
        },
        {
          length: 2,
          height: 15,
          nDoor: 0,
          nWindow: 0,
        },
        {
          length: 2,
          height: 15,
          nDoor: 0,
          nWindow: 0,
        },
      ],
    };
    const response = await supertest(app).post("/calculate").send(data);
    expect(response.status).toBe(400);
    expect(response.body.error).toEqual(
      "Wall height must be 30cm bigger than Door height"
    );
  });
  it("should fail if walls have less than 1m² or more than 50m²", async () => {
    const data: ICalculateRequest = {
      wallMeasurements: [
        {
          length: 0.5,
          height: 1,
          nDoor: 0,
          nWindow: 0,
        },
        {
          length: 2,
          height: 5,
          nDoor: 0,
          nWindow: 0,
        },
        {
          length: 2,
          height: 15,
          nDoor: 0,
          nWindow: 0,
        },
        {
          length: 2,
          height: 15,
          nDoor: 0,
          nWindow: 0,
        },
      ],
    };
    const response = await supertest(app).post("/calculate").send(data);
    expect(response.status).toBe(400);
    expect(response.body.error).toEqual(
      "Walls can't have less than 1m² or more than 50m²"
    );
  });

  it("should fail if door+window area cover more than 50% of wall area", async () => {
    const data: ICalculateRequest = {
      wallMeasurements: [
        {
          length: 2,
          height: 3,
          nDoor: 2,
          nWindow: 2,
        },
        {
          length: 2,
          height: 5,
          nDoor: 0,
          nWindow: 0,
        },
        {
          length: 2,
          height: 15,
          nDoor: 0,
          nWindow: 0,
        },
        {
          length: 2,
          height: 15,
          nDoor: 0,
          nWindow: 0,
        },
      ],
    };
    const response = await supertest(app).post("/calculate").send(data);
    expect(response.status).toBe(400);
    expect(response.body.error).toEqual(
      "Door + Window area must not cover 50%+ of Wall area"
    );
  });
});
