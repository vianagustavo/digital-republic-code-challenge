import supertest from "supertest";
import app from "../../app";

describe("Health Check", () => {
  it("should be able to get status 200 from health check route", async () => {
    const response = await supertest(app).get("/");
    expect(response.status).toBe(200);
  });
});
